## About
GSharp (or G#) is an programming language in development.

## Example
```
/*
 * Functions are defined with:
 * function <name>(<args>) <code>
 *
 * And called with:
 * <name>(<args>);
 */

function main(args)
    io.println("Hello, World!");
```