## Builtin Modules

### io
Module with all in/out functions.

## Builtin Types

### Object
Base for other types, classes and functions.

### Boolean
Logical type, can have only 2 values, true and false.

* Sub functions:
  ```
  function toString()
  ```
  Convert to string.

### Integer
Type that accept numbers from -2.147.483.647 to 2.147.483.647.

* Sub functions:
  ```
  function toString()
  ```
  Convert to string.

### String
Type that accepts an array of characters.

* Sub functions:
  ```
  function size()
  ```
  Return the size of the string.

### List
Type that accept an array of any type.

* Sub functions:

  ```
  function add(item)
  ```
  Add a item to the list.

  ```
  function size()
  ```
  Return the size of the list.